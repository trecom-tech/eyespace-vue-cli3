** This is Single Page App was built with Vue-cli3, buttercms **

## Follows
	"dependencies": {
	    "axios": "^0.18.0",
	    "buttercms": "^1.1.1",
	    "nprogress": "^0.2.0",
	    "papaparse": "^4.6.0",
	    "vue": "^2.5.16",
	    "vue-analytics": "^5.12.3",
	    "vue-concise-slider": "^2.4.8",
	    "vue-loader": "^15.3.0",
	    "vue-resource": "^1.5.1",
	    "vue-router": "^3.0.1",
	    "vue-scrollto": "^2.11.0",
	    "vue-slide-up-down": "^1.3.3",
	    "vue2-google-maps": "^0.10.1",
	    "vuetify": "^1.1.10"
	  },
	"devDependencies": {
		"@vue/cli-plugin-babel": "^3.0.0-rc.10",
		"@vue/cli-plugin-eslint": "^3.0.0-rc.10",
		"@vue/cli-plugin-unit-mocha": "^3.0.0-rc.10",
		"@vue/cli-service": "^3.0.0-rc.10",
		"@vue/eslint-config-airbnb": "^3.0.0-rc.10",
		"@vue/test-utils": "^1.0.0-beta.20",
		"chai": "^4.1.2",
		"eslint": "^5.3.0",
		"eslint-plugin-import": "^2.13.0",
		"node-sass": "^4.9.0",
		"sass-loader": "^7.0.1",
		"vue-lazyload": "^1.2.6",
		"vue-template-compiler": "^2.5.16"
	},
