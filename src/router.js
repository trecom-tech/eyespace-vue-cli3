import Vue from 'vue';
import Router from 'vue-router';

import BlogHome from './components/Practitioner/Blog/BlogHome.vue';
import BlogPost from './components/Practitioner/Blog/BlogPost.vue';
import Contact from './components/Practitioner/Contact.vue';
import Home from './views/Home.vue';
import Intro from './components/Practitioner/Intro.vue';
import PracticeLocator from './views/PracticeLocator.vue';
import Practitioner from './views/Practitioner.vue';

// import EyeSpaceBespokeCare from
// './components/Resource/eyespace-bespoke-care-and-maintenance.vue';
// import EyeSpaceBespokeLenses from './components/Resource/what-are-eyespace-bespoke-lenses.vue';
// import ForgeOrthoK from './components/Resource/what-is-forge-ortho-k.vue';
// import ForgoCare from './components/Resource/forge-care-and-maintenance.vue';
// import Resources from './views/Resource.vue';

Vue.use(Router);

export default new Router({
	mode: 'history',
	base: process.env.BASE_URL,
	scrollBehavior() {
		return { x: 0, y: 0 };
	},
	routes: [
		{
			path: '/',
			name: 'app.home',
			component: Home,
		},
		{
			path: '/practice-locator',
			name: 'app.practice_locator',
			component: PracticeLocator,
		},
		// {
		// 	path: '/resources',
		// 	name: 'app.resources',
		// 	component: Resources,
		// 	children: [
		// 		{
		// 			path: '/resources/forge-care-and-maintenance',
		// 			name: 'resources.forge_care',
		// 			component: ForgoCare,
		// 			meta: {
		// 				title: 'Forge care and maintenance',
		// 			},
		// 		},
		// 		{
		// 			path: '/resources/eyespace-bespoke-care-and-maintenance',
		// 			name: 'resources.eyespace_bespoke_care',
		// 			component: EyeSpaceBespokeCare,
		// 			meta: {
		// 				title: 'Bespoke care and maintenance',
		// 			},
		// 		},
		// 		{
		// 			path: '/resources/what-are-eyespace-bespoke-lenses',
		// 			name: 'resources.eyespace_bespoke_lenses',
		// 			component: EyeSpaceBespokeLenses,
		// 			meta: {
		// 				title: 'What are EyeSpace Bespoke lenses?',
		// 			},
		// 		},
		// 		{
		// 			path: '/resources/what-is-forge-ortho-ks',
		// 			name: 'resources.forge_ortho_ks',
		// 			component: ForgeOrthoK,
		// 			meta: {
		// 				title: 'What is Forge Ortho-K?',
		// 			},
		// 		},
		// 	],
		// },
		{
			path: '/practitioner',
			name: 'app.practitioner',
			component: Practitioner,
			meta: {
				title: 'EyeSpace',
			},
			children: [
				{
					path: '/practitioner/intro',
					name: 'practitioner.intro',
					component: Intro,
				},
				{
					path: '/practitioner/contact',
					name: 'practitioner.contact',
					component: Contact,
				},
				{
					path: '/practitioner/blog',
					name: 'practitioner.blog_home',
					component: BlogHome,
				},
				{
					path: '/practitioner/blog/:slug/',
					name: 'practitioner.blog_post',
					component: BlogPost,
				},
			],
		},
	],
});
