import img3 from '@/assets/images/img3.png';

import img13 from '@/assets/images/img13.png';

import img24 from '@/assets/images/img24.png';

import logo01 from '@/assets/images/logo01.png';

import logo02 from '@/assets/images/logo02.png';

import logo03 from '@/assets/images/logo03.png';

import img4 from '@/assets/images/img4.jpg';

import img5 from '@/assets/images/img5.jpg';

import img6 from '@/assets/images/img6.jpg';

import img7 from '@/assets/images/img7.jpg';

import img8 from '@/assets/images/img8.jpg';

import img9 from '@/assets/images/img9.jpg';

import img10 from '@/assets/images/img10.jpg';

import img11 from '@/assets/images/img11.jpg';

import img15 from '@/assets/images/img15.jpg';

import img16 from '@/assets/images/img16.jpg';

import img17 from '@/assets/images/img17.jpg';

import img18 from '@/assets/images/img18.jpg';

import img19 from '@/assets/images/img19.jpg';

import img20 from '@/assets/images/img20.jpg';

import img21 from '@/assets/images/img21.jpg';

import img22 from '@/assets/images/img22.jpg';

import img25 from '@/assets/images/img25.jpg';

import img26 from '@/assets/images/img26.jpg';

import img27 from '@/assets/images/img27.jpg';

import img28 from '@/assets/images/img28.jpg';

const homeContents = [
	{
		header: {
			title: 'What are Scleral lenses?',
		},
		banner: {
			img: {
				url: logo01,
				alt: 'Scleral EyeSpace',
				width: '406',
				height: '126',
			},
		},
		articles: {
			img: img3,
			subTitle: 'Your key to clear vision',
			subText: 'Scleral lenses help to create clear vision for people who have been told that they are not ideally suited to glasses or contact lenses in the past. They provide all of the necessary elements for good, safe and clear contact lens wear. Your practitioner will advise you on the best use of these. Scleral lenses are a large custom designed hard contact lens that is very comfortable to wear. They are used in the treatment of eye diseases such as keratoconus and are also used in patients with high amounts of astigmatism. They provide excellent vision quality and can be used in professions with dusty environments.',
		},
		info: {
			heading: {
				title: 'Am I a candidate for Scleral Lenses',
				content: 'Do you have any of the following symptoms? You could be a candidate for Scleral lenses',
			},
			list: [{
				overlay: [{
					img: img4,
					isChecked: false,
					title: 'Unstable Vision',
					content: 'My vision is unstable',
				}, {
					img: img5,
					isChecked: false,
					title: 'Distance Vision',
					content: 'My distance vision is poor',
				}, {
					img: img6,
					isChecked: false,
					title: 'Near Vision',
					content: 'My near vision is poor',
				}, {
					img: img7,
					isChecked: false,
					title: 'Dry/Red Eyes',
					content: 'I get dry/red eyes with soft contact lenses',
				}],
			}, {
				overlay: [{
					img: img8,
					isChecked: false,
					title: 'Keratoconus',
					content: 'I have keratoconus',
				}, {
					img: img9,
					isChecked: false,
					title: 'Astigmatism',
					content: 'I have astigmatism',
				}, {
					img: img10,
					isChecked: false,
					title: 'Allergies',
					content: 'I have allergies',
				}, {
					img: img11,
					isChecked: false,
					title: 'Uncomfortable Lenses',
					content: 'I find hard lenses/ RGP’s uncomfortable',
				}],
			}],
		},
	},
	{
		header: {
			title: 'What is Forge Ortho-K?',
		},
		banner: {
			img: {
				url: logo02,
				alt: 'forge ortho-k',
				width: '208',
				height: '134',
			},
		},
		articles: {
			img: img13,
			subTitle: 'Clear vision while you sleep',
			subText: 'Forge Ortho-K contact lenses are specially designed to be worn whilst sleeping. The design gently reshapes the front of the eye overnight so that on removal in the morning, vision is clear without the need to wear glasses.',
		},
		info: {
			heading: {
				title: 'Am I a candidate for Ortho-K?',
				content: 'Do you have any of the following symptoms? You could be a candidate for Ortho-k overnight vision correction',
				extra: 'Freedom from....',
			},
			list: [{
				overlay: [{
					img: img15,
					isChecked: false,
					title: 'Distance Vision',
					content: 'My distance vision is getting worse',
				}, {
					img: img16,
					isChecked: false,
					title: 'Distance Vision',
					content: 'My distance vision is poor',
				}, {
					img: img17,
					isChecked: false,
					title: 'Near Vision',
					content: 'My near vision is poor',
				}, {
					img: img18,
					isChecked: false,
					title: 'Astigmatism',
					content: 'I have astigmatism',
				}],
			}, {
				overlay: [{
					img: img19,
					isChecked: false,
					title: 'Laser Surgery',
					content: 'I want an alternative to laser surgery',
				}, {
					img: img20,
					isChecked: false,
					title: 'Laser/Glasses',
					content: 'I have had laser surgery and I think I need glasses',
				}, {
					img: img21,
					isChecked: false,
					title: 'Free of Contacts/Glasses',
					content: 'I want to be free of contacts or glasses during the day',
				}, {
					img: img22,
					isChecked: false,
					title: 'Myopia Control',
					content: 'I have Myopia',
				}],
			}],
		},
	},
	{
		header: {
			title: 'What are EyeSpace Bespoke lenses?',
		},
		banner: {
			img: {
				url: logo03,
				alt: 'EyeSpace by innovatus technology',
				width: '256',
				height: '53',
			},
		},
		articles: {
			img: img24,
			subTitle: 'Match the contour of your eye',
			subText: 'EyeSpace Bespoke lenses are hard contact lenses. Optometrists that fit these lenses use a technology called corneal topography to map the contour of your eye. Think of this like your fingerprint. A fingerprint is unique to you and as such you need a customized contact lens to provide the best fit. Our lenses provide an bespoke contact lens option that provide you with the best quality vision. So why not treat yourself to the best?',
		},
		info: {
			heading: {
				title: 'Am I a candidate for EyeSpace Bespoke lenses',
				content: 'Do you have any of the following symptoms? You could be a candidate for EyeSpace Bespoke lenses',
				extra: 'If you have any of the following, you are a candidate for EyeSpace Bespoke lenses',
			},
			list: [{
				overlay: [{
					img: img25,
					isChecked: false,
					title: 'Long Sighted',
					content: 'I am long sighted',
				}, {
					img: img26,
					isChecked: false,
					title: 'Short Sighted',
					content: 'I am short sighted',
				}, {
					img: img27,
					isChecked: false,
					title: 'Astigmatism',
					content: 'I have astigmatism',
				}, {
					img: img28,
					isChecked: false,
					title: 'Vision Quality',
					content: 'I want better quality vision',
				}],
			}],
		},
	},
];

export default homeContents;
