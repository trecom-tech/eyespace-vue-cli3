import './assets/scss/main.scss';
// import axios from '../node_modules/axios';
import Nprogress from '../node_modules/nprogress';
import Vue from '../node_modules/vue';
import VueAnalytics from '../node_modules/vue-analytics';
import VueLazyload from '../node_modules/vue-lazyload';
import VueResource from '../node_modules/vue-resource';
import VueSlideUpDown from '../node_modules/vue-slide-up-down';
import * as VueGoogleMaps from '../node_modules/vue2-google-maps';
import router from './router';
import App from './App.vue';

// import '../node_modules/vuetify/dist/vuetify.min.css'; // Ensure you are using css-loader
// import Vuetify from '../node_modules/vuetify';
// Vue.use(Vuetify);

const VueScrollTo = require('vue-scrollto');

Vue.use(VueScrollTo);

Vue.use(VueLazyload);

Vue.use(VueResource);

Vue.component('vue-slide-up-down', VueSlideUpDown);

// Vue.http.headers.common['Access-Control-Allow-Origin'] = '*';
// Vue.http.headers.common['Access-Control-Request-Method'] = '*';

Vue.use(VueAnalytics, {
    id: 'UA-122881451-2',
    disabled: true,
});

Vue.use(VueGoogleMaps, {
    load: {
        key: 'AIzaSyBR1HFXhYxzvj9_N5RiU_YmiK7dBOiIUos',
        libraries: 'places,drawing,visualization',
    },
});

Nprogress.configure({ showSpinner: false });
window.Nprogress = Nprogress;

router.beforeEach((to, from, next) => {
    window.Nprogress.start();
    // scroll to top when changing pages
    // if (document.scrollingElement) {
    //     document.scrollingElement.scrollTop = 0;
    // } else if (document.documentElement) {
    //     document.documentElement.scrollTop = 0;
    // }
    next();
});

router.afterEach(() => {
    window.Nprogress.done();
});

Vue.config.productionTip = false;

new Vue({
    router,
    render: h => h(App),
}).$mount('#app');
