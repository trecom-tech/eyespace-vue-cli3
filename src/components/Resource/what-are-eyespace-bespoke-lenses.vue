<template>
  <div id="eyespace_bespoke_lenses">
    <section class="content resources-article">
      <article class="bespoke">
        <h1>What are
          <strong>Bespoke Lenses?</strong>
        </h1>
        <figure class="float-right">
          <img src="@/assets/images/patient.png" alt="Patient" width="180">
        </figure>
        <p>Bespoke are custom-made Rigid Gas Permeable contact lenses designed to rest on the
          cornea, holding their shape
          to create an even optical surface. These are ideal for high prescriptions and a number of
          eye conditions.</p>
        <p>RGP corneal lens wearers experience clear, high quality vision with a safety profile
          higher than any other contact lens.</p>
        <div class="clear"></div>
        <h2>Why should I use an RGP corneal lens?</h2>
        <h3>Eye disease</h3>
        <p>A variety of eye conditions are best managed with rigid gas permeable (RGP) contact
          lenses.
          <sup>1</sup> These include keratoconus, pellucid marginal degeneration, corneal grafts,
          corneal dystrophies, corneal
          scarring from injury, and altered eye surfaces following corneal refractive surgery.</p>
        <p>One of the most common conditions fitted with rigid lenses is keratoconus. The irregular
          distortion of the cornea
          limits the quality of vision possible with spectacles or soft contact lenses.
          <sup>2</sup>
        </p>
        <figure class="float-left">
          <img src="@/assets/images/eye-disease.png" alt="Eye disease" width="180">
        </figure>
        <p>
          <strong>Wearing a smooth, rigid contact lens over the cornea allows tears to fill in the
            space between the rigid lens
            surface and the corneal irregularity, neutralising most of the corneal
            distortion.</strong>
        </p>
        <p>Neutralising corneal distortion gives wearers better definition and contrast with
          improved vision and decreased ghosting.
          <sup>2</sup> Similar benefits are achieved for the irregular corneal surfaces present in
          all of the corneal diseases
          mentioned above.</p>
        <div class="clear"></div>
        <h3>High prescriptions</h3>
        <p>Patients with high refractive errors such as astigmatism, myopia (short- sightedness) or
          hyperopia (long-sightedness)
          may not always achieve satisfactory vision with spectacles or soft contact lenses.
          <sup>4</sup>
        </p>
        <figure class="float-left">
          <img src="@/assets/images/myopia.png" alt="High prescriptions" width="180">
        </figure>
        <p>
          <strong>Rigid lens material can provide greater vision and clarity.</strong>
        </p>
        <p>RGP corneal lenses can be a great option for those needing rigid lenses but finding a
          larger scleral lens diﬃcult to handle, insert and remove.</p>
        <div class="clear"></div>
        <h2>How do RGP corneal lenses work?</h2>
        <p>RGP corneal lenses are typically smaller than other contact lenses, being only 8.00 to
          11.50 mm in diameter. They
          are designed to ‘ﬂ oat’ on the tears overlaying the cornea, spreading the pressure across
          the corneal surface.</p>
        <figure class="float-left">
          <img src="@/assets/images/safe-lenses.png" alt="Safe lenses" width="180">
        </figure>
        <p>
          <strong>Did you know RGPs are the safest contact lenses you can wear?</strong>
        </p>
        <p>With each blink, the contact lens is designed to move vertically on the eye and then
          settle back into position.
          This movement allows fresh tears to ﬂ ow in under the lens and nourish the cornea.</p>
        <p>RGPs were the first type of lens fitted by optometrists before soft lenses gained
          popularity.
          <sup>3</sup> Though they can take longer to get used to than soft lenses or large scleral
          lenses, once the lids and
          corneal surface adapt, wear is quite comfortable. Most of our rigid corneal lens patients
          wear their lenses for
          long periods during the day due to the comfort and excellent vision they provide.</p>
        <p>Lens complications, though minimal when instructions are followed, include:</p>
        <ul>
          <li>Mild redness and irritation initially during wear.</li>
          <li>Risk of corneal damage due to ill-fitting or dirty lenses (it is important to maintain
            proper cleaning and regular consultation).
          </li>
          <li>Reaction to lens solutions, requiring a change to a different type of solution.
            <sup>3</sup>
          </li>
        </ul>
        <div class="clear"></div>
        <h2>How are my lenses designed?</h2>
        <p>At your initial appointment, we will take a range of measurements in each eye, including
          corneal topography, which
          analyses the front shape of your eye. This data is then imported into our lens simulation
          software, EyeSpace by Innovatus Technology, to design your lenses.</p>
        <p>EyeSpace is used by contact lens practitioners across the world including Australia, USA,
          New Zealand and South
          Africa. EyeSpace customises each lens to your prescription and eye shape to a degree of
          accuracy smaller than
          one micron. Your optometrist will make changes to this lens until it looks optimal in the
          simulation before ordering.</p>
        <h2>What will happen at my appointments?</h2>
        <h3>Delivery and teach</h3>
        <p>If you have never worn contact lenses, it can be helpful to practice some eye touching
          techniques before your appointment.
          Wash your hands thoroughly to begin.</p>
        <ul>
          <li>Open the top and bottom eyelids wide with your middle fingers.</li>
          <li>Looking up, gently touch the white of the eye with your index finger.</li>
          <li>Practise inserting lubricating eye drops, holding your eye open.</li>
        </ul>
        <p>At your appointment, your optometrist will examine the design and fit of your EyeSpace
          Bespoke RGP lens on the
          eye. Our team will then teach you how to insert, remove, clean and care for your
          lenses.</p>
        <h2>Frequently asked questions</h2>
        <ul>
          <li>
            <strong>Can the rigid contact lenses damage my eyes?</strong>
          </li>
        </ul>
        <p>RGPs are the safest contact lenses. Any contact lenses have the potential to cause
          infection and vision loss. Hygiene,
          lens care and timely replacement minimises these risks.</p>
        <p>Regular reviews are critical as long-term wear of poorly fitting lenses may cause harm to
          your cornea especially
          in progressive conditions such as keratoconus where corneal changes can occur over
          time.</p>
        <ul>
          <li>
            <strong>Can I sleep in my rigid lenses?</strong>
          </li>
        </ul>
        <p>Please remember that your lenses have to be sterilised and cleaned each day to reduce the
          risk of infection, so nightly wear is usually not recommended.
          <br/> If you would like to sleep in your rigid lenses, then please advise your optometrist
          to see if you are eligible for extended wear.</p>
        <ul>
          <li>
            <strong>I am over 45 and need multifocals or reading glasses. Can I still wear rigid
              corneal lenses?</strong>
          </li>
        </ul>
        <p>Absolutely. There are a number of ways to achieve reading and distance vision with rigid
          contact lenses. Ask your optometrist if these are right for you.</p>
        <ul>
          <li>
            <strong>How often will I have to replace my rigid contact lenses?</strong>
          </li>
        </ul>
        <p>We recommend replacing lenses every one to two years, depending on their condition. This
          is to decrease the chance
          of eye infection or inﬂ ammation and to ensure the optical surface remains smooth and
          clear.</p>
        <ul>
          <li>
            <strong>What ongoing costs are required for rigid contact lens wear?</strong>
          </li>
        </ul>
        <p>You will need regular solutions and insertion drops. We also recommend a spare set of
          lenses.</p>
        <ul>
          <li>
            <strong>How do I find out if I am suitable for RGP lenses?</strong>
          </li>
        </ul>
        <p>A full eye exam with us is needed, even if you have had a recent eye examination at
          another optometrist. This is
          to examine eye health, vision and corneal shape to establish if your eyes are suitable. If
          you have had a full
          eye exam with us in the last 12 months, you may just need a corneal topography and a
          discussion.</p>
        <ol>
          <li>Contact lens management of keratoconus. 2015 Jul;98(4):299–311.</li>
          <li>Rigid gas-permeable contact lens related life quality in keratoconic patients with
            different grades of severity. 2015 Mar;98(2):150–4.
          </li>
          <li>Rabinowitz YS. Keratoconus. Survey of ophthalmology. 1998
            <br/> 4 Shaughnessy MP, Ellis FJ, Jeffery AR, Szczotka L. Rigid Gas-Permeable Contact
            Lenses Are a Safe and Effective
            Means of Treating Refractive Abnormalities in the Pediatric Population. Eye &amp;
            Contact Lens: Science &amp; Clinical Practice. 2001 Oct 1;27(4):195.
          </li>
          <li>Wang Y, Qian X, Zhang X, Xia W, Zhong L, Sun Z, et al. Plasma surface modification of
            rigid contact lenses decreases
            bacterial adhesion. Eye Contact Lens. 2013 Nov;39(6):376–80.
          </li>
        </ol>
      </article>
    </section>
  </div>
</template>

<script>
    export default {
        name: 'what-are-eyespace-bespoke-lenses',
    };
</script>

<style scoped>

</style>
