<template>
  <div id="eyespace_bespoke">
    <section class="content resources-article">
      <article class="bespoke">
        <h1>Care &amp; Maintenance of <strong>Bespoke Lenses</strong></h1>
        <h2>Safety and Hygiene:</h2>
        <p>Rigid Gas Permeable (RGP) contact lenses are recognised as a safe form of vision
          correction.<sup>1</sup>
          However, incorrect care of contact lenses and solutions can increase the risk of eye
          infections and corneal
          ulcers. Risk factors for contact lens related corneal infection include improper lens
          cleaning and
          disinfection, poor hygiene practices and smoking.<sup>2</sup> Following your
          practitioner’s hygiene regime,
          along with regular reviews, will minimise this risk.</p>
        <p>Always thoroughly wash your hands with an antibacterial based hand wash and dry them with
          a clean lint free
          towel before handling, removing or inserting your lenses. Make sure to clean all parts of
          your hands,
          including between the fingers and dry thoroughly.</p>
        <ul>
          <li>Sit at a table or desk and place a lint free cloth down to insert and remove lenses.
            Avoid bathrooms as
            they often contain more germs than any other room in the home.
          </li>
          <li>Inspect your lenses for deposits and defects such as chips or cracks. Do not wear
            chipped or cracked
            lenses as these may cause discomfort and pain. If you notice any defects or if you are
            unsure whether your
            lenses are damaged do not wear the lenses and ask your optometrist to inspect them
            first.
          </li>
          <li>If you misplace a lens, contact your optometrist for a replacement.</li>
        </ul>
        <h3>Things to remember!</h3>
        <ul>
          <li>Your lenses should never come into contact with tap or bottled water. Water contains
            microorganisms like
            Acanthamoeba that can cause sight-threatening eye infections.
          </li>
        </ul>
        <h2>Removal:</h2>
        <p><em>Remove lens either manually or with a suction cup.</em></p>
        <h3>Method 1 - suction cup</h3>
        <figure class="float-right"><img src="@/assets/images/suction-cup.png" alt="Suction cup"
                                         width="180"></figure>
        <ul>
          <li>Using your middle fingers, open the eyelids wider than the lens diameter.</li>
          <li>With the suction tool between your index finger and thumb, align the suction cup so it
            is positioned in
            front of and parallel to the lens.
          </li>
          <li>When the suction cup touches the lens, apply gentle pressure to adhere the lens to the
            cup and remove the lens from your eye.
          </li>
          <li>After removal from the eye, carefully slide the lens sideways from the suction cup and
            it will come off easily.
          </li>
        </ul>
        <div class="clear"></div>
        <h3>Method 2 - manual removal</h3>
        <figure class="float-right"><img src="@/assets/images/manual-removal-2.png"
                                         alt="Manual removal example #2"
                                         width="180"></figure>
        <figure class="float-right"><img src="@/assets/images/manual-removal-1.png"
                                         alt="Manual removal example #1"
                                         width="180"></figure>
        <ul>
          <li>Using your middle fingers, open the lids wider than the lens diameter.</li>
          <li>Apply pressure to the lid margins, pushing in together to move your lids under the
            lens and lever it out of the eye.
          </li>
        </ul>
        <div class="clear"></div>
        <h3>Things to remember!</h3>
        <ul>
          <li>Do not modify the recommended cleaning routine or solutions without consulting your
            optometrist. Other
            solutions may not be compatible with your eyes and lenses and may cause discomfort or
            allergic reactions.
          </li>
          <li>Shortcuts with cleaning solutions may appear to save money but may result in
            ineffectively cleaning and
            disinfecting the lens. Incorrect cleaning solutions may damage your lenses or lead to an
            eye infection which can result in vision loss.
          </li>
          <li>Never clean or store your rigid lens with soft contact lens solutions. These products
            work in a different
            way to the rigid lens solutions and will not clean and condition your rigid lenses as
            well.
          </li>
          <li>Replace your lens case every time you start a new bottle of lens cleaner to avoid
            microbial contamination and loss of effectivity.
          </li>
          <li>To avoid contamination do not touch the tips of solution bottles. Replace caps after
            use.
          </li>
          <li>If your eyes are very painful after hours, consult your local hospital or emergency
            eye clinic.
          </li>
          <li>If you notice scratches, chips or misplace a lens, contact your optometrist for a
            replacement. Do not wear
            chipped or cracked lenses as these may cause discomfort and pain.
          </li>
        </ul>
        <h2>Cleaning:</h2>
        <h3>Peroxide cleaning systems</h3>
        <p>Place lenses in the supplied basket holder and fill the case with the hydrogen peroxide
          solution. If
          required, add the neutralising tablet, replace the lid and tighten. It is imperative the
          lenses are not removed before the solution has had 6 hours to neutralise.</p>
        <h3>Two-step cleaner</h3>
        <p>As directed by your optometrist, some patients require a two-step lens cleaning system.
          Hold the lens in the palm of your hand and instil one drop of the daily lens cleaner into
          the back of the lens.<sup>10</sup> Gently rub the lens until the liquid foams like soap.
          Then rinse offwith saline and place the lens into the conditioning solution to store
          overnight.</p>
        <h3>Intensive cleaner</h3>
        <p>To clean and maintain the wettability of your lenses use intensive cleaner, such as
          Menicon Progent. Place
          the lenses into the contact lens case holders. Open vial A and B by twisting the cap and
          pour the contents
          into the contact lens case. Replace the lid and tighten. Leave the lenses in the solution
          for 30 minutes, then
          remove and rinse thoroughly with saline. Lenses can now be worn or soaked in your daily
          cleaner (please note
          that this product may not be available in all countries, ask your practitioner).</p>
        <h4>Things to <strong>remember!</strong></h4>
        <ul>
          <li>The first thing you will notice about your lenses is they have different colours. A
            green or grey lens for
            your right eye and blue lens for your left eye will ensure you don’t get the lenses
            confused. A good way to
            remember this is that the second letter of green/grey is R for right! The second letter
            of blue is L for
            left!
          </li>
        </ul>
        <p>EyeSpace Bespoke lenses are manufactured from a durable gas permeable polymer which
          resists wear and tear
          during normal lens wearing circumstances. However, it is still important to handle them
          carefully.</p>
        <p>Habits that may cause a lens to break include:</p>
        <figure class="float-right"><img src="@/assets/images/break-pressure.png"
                                         alt="Excessive pressure on the lens"
                                         width="180"></figure>
        <ul>
          <li>Pressure on the lens. If the lens lands on a mirror or flat surface, gently slide it
            off to the edge of
            the surface or use a suction cup to remove it from the surface.
            <div class="clear"></div>
            <figure class="float-right"><img src="@/assets/images/break-pull.png"
                                             alt="Pulling the lens too firmly"
                                             width="180"></figure>
          </li>
          <li>Removing a lens too firmly from the suction tool. Slide the lens off the suction point
            instead.
            <div class="clear"></div>
            <figure class="float-right"><img src="@/assets/images/break-bend.png"
                                             alt="Forcing the lens to bend"
                                             width="180"></figure>
          </li>
          <li>Forcing the lens to bend while cleaning. Some force is required to clean a rigid lens
            using your cleaning
            solution, but not much - the friction of your skin surface will do most of the job. If
            you are pressing down
            on both sides of the lens, it will flex and eventually snap if too much force is
            applied.
            <div class="clear"></div>
          </li>
        </ul>
        <h2>Insertion:</h2>
        <div class="clear"></div>
        <figure class="float-left"><img src="@/assets/images/step-1.png" alt="Step #1" width="80">
        </figure>
        <h3>Step 1:</h3>
        <p>Remove lenses from the <strong>Cleaning Solution Case</strong> and rinse with saline. If
          using hydrogen
          peroxide solution a minimum soaking time of 6 hours is required for the acid to neutralise
          to saline.</p>
        <div class="clear"></div>
        <figure class="float-left"><img src="@/assets/images/step-2.png" alt="Step #2" width="80">
        </figure>
        <h3>Step 2:</h3>
        <p>Place 1-2 drops of a <strong>Lubricating Eye Drop</strong> in the back of the lens. Place
          the lens on your
          index finger and insert directly onto the centre (coloured part) of your eye in a face-
          down position. To save
          confusion, it’s a good idea to always insert the right lens first.</p>
        <div class="clear"></div>
        <figure class="float-left"><img src="@/assets/images/step-3.png" alt="Step #3" width="80">
        </figure>
        <h3>Step 3:</h3>
        <p>If you happen to drop your lens, use <strong>Saline Solution</strong> to rinse. Place the
          lens in the palm of
          your hand and thoroughly rinse for 5 seconds.</p>
        <div class="clear"></div>
        <figure class="float-left"><img src="@/assets/images/step-4.png" alt="Step #4" width="80">
        </figure>
        <h3>Step 4:</h3>
        <p>Dry and wipe out your lens case with a tissue. Leave the case lid off to air dry while
          wearing your
          lenses.</p>
        <div class="clear"></div>
        <h3>Things to <strong>remember!</strong></h3>
        <p><strong>NORMAL</strong> occurrences on insertion:</p>
        <p>Occasionally a lens may become dislodged within your eye. This may be uncomfortable
          however it won’t do any
          harm.</p>
        <ul>
          <li>Look in a mirror to establish where the lens has moved to.</li>
          <li>Move your eyes in a direction away from where the lens is located. For example, if the
            lens in your right
            eye is on the white of your eye nearest to your nose, move your eyes to the right.
            Gently nudge the lens
            with your eyelids. Avoid directly pushing the lens into place as this can damage the
            surface of your eye.
          </li>
        </ul>
        <p><strong>ABNORMAL</strong> occurrences on insertion:</p>
        <ul>
          <li>If on insertion you experience stinging, burning or pain, remove the lens, rinse and
            re-insert. If
            problems persist, remove the lens and see your optometrist at your earliest convenience.
          </li>
        </ul>
        <h3>During the first 2 weeks:</h3>
        <p><strong>NORMAL</strong> occurrences include:</p>
        <ul>
          <li>Foreign body sensation and slight discomfort whilst wearing lenses.</li>
          <li>Mild burning, stinging and watering which resolves once the lens is removed, rinsed
            with saline and re-inserted.
          </li>
        </ul>
        <p><strong>ABNORMAL</strong> symptoms (contact your optometrist immediately) include:</p>
        <ul>
          <li>Redness, discharge, pain, light sensitivity, burning, stinging, and excessive watering
            of the eyes which do not resolve after lenses are removed.
          </li>
          <li>Feeling like the lens is stuck to the eye and having dificulty removing.</li>
        </ul>
        <h3>Things to remember!</h3>
        <ul>
          <li>Do not modify the recommended cleaning routine or solutions without consulting your
            optometrist. Other
            solutions may not be compatible with your eyes and lenses and may cause discomfort or
            allergic reactions.
          </li>
          <li>Shortcuts with cleaning solutions may appear to save money but may result in ineﬀ
            ectively cleaning and
            disinfecting the lens. Incorrect cleaning solutions may damage your lenses or lead to an
            eye infection which can result in vision loss.
          </li>
          <li>Never clean or store your rigid lens with soft contact lens solutions. These products
            work in a diﬀ erent
            way to the rigid lens solutions and will not clean and condition your rigid lenses as
            well.
          </li>
          <li>Replace your lens case every time you start a new bottle of lens cleaner to avoid
            microbial contamination and loss of eﬀ ectivity.
          </li>
          <li>To avoid contamination do not touch the tips of solution bottles. Replace caps after
            use.
          </li>
          <li>If your eyes are very painful after hours, consult your local hospital or emergency
            eye clinic.
          </li>
          <li>If you notice scratches, chips or misplace a lens, contact your optometrist for a
            replacement. Do not wear
            chipped or cracked lenses as these may cause discomfort and pain.
          </li>
        </ul>
        <h2>Contraindictions</h2>
        <ul>
          <li>Do not use your Bespoke contact lenses in the following cases: *</li>
          <li>Acute inflammation or infection of the anterior chamber of the eye</li>
          <li>Any eye disease, injury, or abnormality that affects the cornea, conjunctiva or
            eyelids
          </li>
          <li>Severe insufficiency of tears or inflammatory dry eye</li>
          <li>Corneal hypoesthesia (reduced corneal sensitivity)</li>
          <li>Any systemic disease which may affect the eye or be exacerbated by wearing contact
            lenses
          </li>
          <li>Allergic reactions to ocular surfaces or adnexa which may be induced or exaggerated by
            wearing contact lenses or use of contact lens solutions
          </li>
          <li>Allergy to any ingredient, such as peroxide and hydraglyde in a recommended cleaning
            solution
          </li>
          <li>Any active corneal infection (bacterial, fungal or viral).</li>
          <li>Red or irritated eyes.</li>
          <li>Remember your eye must:
            <blockquote>
              <ul>
                <li>Look good (no red eyes)</li>
                <li>Feel good (no pain, discomfort or light sensitivity)</li>
                <li>See good (no persistent blurry vision)</li>
                <li><strong>If in doubt, take them out and call your optometrist</strong></li>
              </ul>
            </blockquote>
          </li>
        </ul>
        <ol>
          <li>Bailey CS et al. A review of relative risks associated with four types of contact
            lenses. Cornea, (1990) Journals.lww.com
          </li>
          <li>Ladage PM, Yamamoto K, Ren DH, Li L, Jester JV, Petroll WM, et al. Effects of rigid
            and soft contact lens
            daily wear on corneal epithelium, tear lactate dehydrogenase, and bacterial binding to
            exfoliated epithelial
            cells. OPHTHA. Elsevier; 2001 Jul 1;108(7):1279–88.
          </li>
        </ol>
      </article>
    </section>
  </div>
</template>

<script>
    export default {
        name: 'eyespace-bespoke-care-and-maintenance',
    };
</script>

<style scoped>

</style>
