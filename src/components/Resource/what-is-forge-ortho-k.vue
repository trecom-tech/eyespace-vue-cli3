<template>
  <div id="forge_ortho_ko">
    <section class="content resources-article">
      <article class="ortho-k">
        <h1>What is <strong>Orthokeratology?</strong></h1>
        <p>Orthokeratology, or Ortho-K for short, is a non-surgical and reversible procedure in
          which custom-designed contact lens devices are used to gently reshape the cornea (front
          surface) of your eye. It is also known as Corneal Reshaping Technology (CRT) and lenses
          are worn while sleeping, giving you clear vision during the day without the aid of glasses
          or contact lenses. Orthokeratology can correct refractive errors like nearsightedness
          (myopia), farsightedness (hyperopia), astigmatism and can improve near vision after age 42
          (presbyopia).</p>
        <p><strong>Orthokeratology may sound dramatic, but it is quite simple. ‘Ortho’ means to
          ‘correct’. Orthodontics correct teeth. Orthopaedics correct the musculoskeletal system.
          Orthokeratology corrects the refractive error by reshaping the cornea.</strong></p>
        <p>Orthokeratology can be effective on farsighted, nearsighted (myopic), and astigmatic
          patients. Ideally, your prescription should be in the range of +2.00 to -6.00 with no more
          than -1.75 diopters of surface astigmatism. In some cases your Orthokeratologist will
          attempt higher prescriptions.<sup>1</sup></p>
        <p>Orthokeratology technology continues to evolve, and in many cases, Ortho-K can also
          successfully correct corneas with high astigmatism, presbyopia and Post-LASIK (refractive
          surgery) with residual myopia or irregular treatment areas.</p>
        <p>The Forge Ortho-K lenses are custom designed with our cutting-edge EyeSpace contact lens
          design software. Using the corneal topography map taken by your eye care practitioner,
          EyeSpace enables the calculation and computerised simulation of your custom made lens,
          without the need for time consuming and uncomfortable in oﬃ ce trial fitting.</p>
        <h2>How does Ortho-K work?</h2>
        <h3>Lenses that work when you aren't wearing them</h3>
        <p>Your custom-designed EyeSpace Forge Ortho-K contact lens reshape the front surface of the
          eye while you sleep. It uses the forces of the eyelid and tear ﬂ uid beneath the lens to
          reshape the top layers of the cornea.</p>
        <p>For treatment of myopia, the lens produces a ﬂ atter central cornea, correcting near-
          sightedness by decreasing the power of the eye.</p>
        <p>For hyperopia and presbyopia, the design of the lens steepens the central cornea,
          correcting long-sightedness by increasing the power of the eye.<sup>3,4,5,6,7</sup></p>
        <h3>Immediate improvement</h3>
        <p>Improvements in vision can usually be seen the next day, with the procedure stabilising
          after a week. With high myopia over -6.00D, astigmatism and hyperopia the process can take
          two to four weeks to achieve full correction. Ortho-K does not permanently change the
          shape of the eye. Your eyes’ sh ape will revert back to their original state within one to
          four weeks if you stop wearing the lenses.<sup>8</sup></p>
        <h3>Comfort and safety</h3>
        <p>Ortho-K lenses are surprisingly comfortable to wear. Most people comment that they forget
          they have lenses in after the first week of wear. Similar to disposable soft contact
          lenses, the main adverse event associated with Ortho-K corneal reshaping therapy is an
          infection of the cornea, microbial keratitis.<sup>9</sup> With proper care and
          maintenance, the risk of inﬂ ammation and bacterial infections due to poor hygiene can be
          minimised.</p>
        <h2>Who can benefit from Ortho-K?</h2>
        <p>Ortho-K holds particular appeal for certain groups of people:</p>
        <ul>
          <li><strong>People who play sports — especially contact and water sports</strong></li>
        </ul>
        <p>You can enjoy the freedom of playing sports without worrying about your glasses getting
          damaged or your contact lenses falling out. Ortho-K lenses are only worn at night while
          you sleep, leaving you with perfect vision during the day on the sports field, court or in
          the swimming pool.</p>
        <ul>
          <li><strong>People working in dusty or dirty environments</strong></li>
        </ul>
        <p>Dusty or dirty environments can play havoc with contact lenses and glasses. If you’re a
          tradesman who needs to squeeze into tight spaces or work in dusty work conditions, Ortho-K
          lenses will allow you to see clearly and get on with the job.</p>
        <ul>
          <li><strong>People with dry eyes or allergies</strong></li>
        </ul>
        <p>Ortho-K lenses are great if you suffer from allergies or dry eyes and work long hours on
          the computer or in air conditioned environments and don’t wish to wear glasses or contact
          lenses.</p>
        <ul>
          <li><strong>People who want freedom from their glasses or day wear contact lenses</strong>
          </li>
        </ul>
        <figure class="float-right"><img src="@/assets/images/children.png"
                                         alt="Children with progressive myopia" width="180">
        </figure>
        <ul>
          <li><strong>Children with progressive myopia</strong></li>
        </ul>
        <p>Ortho-K lenses are an excellent option for children with progressive myopia (short
          sightedness). Ortho-K is scientifically proven to dramatically slow, or potentially halt
          the progression of myopia and can have a positive impact on the long-term health of your
          child’s eye.10 Wearing Ortho-K lenses only at night gives kids the freedom to run around,
          play sports and wear fashion sunglasses during the day without worrying about their
          glasses or contact lenses in the eye.</p>
        <div class="clear"></div>
        <p>In short, Ortho-K works best for people who don’t want to or are unable to wear glasses
          or contact lenses during the day but don’t mind wearing lenses while they sleep.</p>
        <p>People with healthy corneas who are unsuitable for laser surgery (LASIK) can still
          qualify for Ortho-K.</p>
        <p>Children and teenagers especially benefit from the freedom to play sport and wear fashion
          sunglasses. Parents love the fact that there is no risk of kids losing their contact
          lenses or spectacles.</p>
        <p>Ortho-K is an excellent option for those with youth-onset myopia due to the additional
          benefits of being able to slow and even halt the progression of myopia. Contact us to book
          an appointment to see if you qualify for Ortho-K.</p>
        <h2>How are the lenses designed?</h2>
        <div class="clear"></div>
        <figure class="float-right"><img src="@/assets/images/design.png" alt="Lens design"
                                         width="180"></figure>
        <p>The orthokeratology procedure is performed with the use of a medical instrument called a
          corneal topographer. The topographer measures the shape or topography of your cornea,
          enabling us to calculate and create a computerised simulation of the Forge Ortho-K lens.
          Corneal topography is used at each appointment to map the shape of the eye, allowing the
          measurement of the change in optical power of the cornea.</p>
        <p>At your initial appointment, we will take a range of measurements from each eye. This
          data is imported into our simulation software, EyeSpace, which is then used to design your
          Forge Ortho-K lenses.</p>
        <p>Contact lens practitioners internationally use EyeSpace to customise each lens to your
          prescription and individual eye shape to micron accuracy. Your eye care practitioner will
          calculate, design and simulate your lens without the need for time consuming and
          uncomfortable trial fitting prior to ordering and manufacturing the lens.</p>
        <p>This simulation process means that more complex prescriptions can be treated with the
          Forge Ortho-K lenses and provides the greatest chance of success with fewer fitting
          appointments and less lens changes.</p>
        <div class="clear"></div>
        <h2>What will happen at Ortho-K appointments?</h2>
        <h3>Preparation for contact lens wear</h3>
        <p><strong>If you have never worn contact lenses, it can be helpful to practice holding your
          eyelids, touching your eyes, and applying eye drops prior to your appointment.</strong>
        </p>
        <p>Wash your hands thoroughly prior to practising these techniques:</p>
        <ul>
          <li>Hold the top and bottom eyelids and lashes wide open with the middle finger of each
            hand. Look up and using the index finger on your hand that is holding your bottom eyelid
            and lashes, gently touch the white part of your eye.
          </li>
          <li>Practice inserting lubricating eye drops, holding your eye open as the drop goes in.
            When the drop touches the eye, try not to blink. It will be hard not to blink at first,
            however, with time you will desensitise to the feeling.
          </li>
        </ul>
        <h3>Delivery and lens instruction appointment</h3>
        <p>At the lens delivery appointment, your optometrist will examine the fit and optics of the
          lens on the eye. You will then be taught how to insert, remove, clean and care for your
          lenses.</p>
        <p>You will usually start Ortho-K wear the night of your delivery and lens instruction
          appointment. Insert your lenses 10 to 15 minutes before going to bed to allow time for the
          lens to settle. For best results sleep on your back. You will notice a foreign body
          sensation with the lens on your eye when you blink and with your eyes open. This will
          improve with time.</p>
        <h3>First morning review</h3>
        <p><strong>Your vision may be clearer; about 50% of the total refractive change occurs after
          just one night of lens wear.</strong></p>
        <p>Your eyes may be sensitive when you wake up. Take the lenses out, following the provided
          removal instructions, and the symptoms should improve. You may still need to use your
          glasses to drive to your appointment, although they may not work. If possible we recommend
          organising someone to drive you to this appointment.</p>
        <p>Your optometrist will review your insertion and removal techniques and assess your vision
          changes and eye health. Corneal topography will determine how well the Forge
          orthokeratology lens is reshaping your eye. If your vision ﬂ uctuates this can be from an
          incorrect insertion technique or an air bubble at insertion the night before, so remember
          to mention this to your Optometrist.</p>
        <p>The next appointment will usually be booked within one to two weeks. Most patients will
          need some top up vision correction during the first week. Old glasses with lower
          prescriptions can be helpful. Your optometrist can also provide loan spectacles or soft
          contact lenses to use in the transitional phase of the Ortho-K corneal reshaping.</p>
        <h3>One week review</h3>
        <p><strong>Many patients will find their vision is nearly 100%.</strong></p>
        <p>Your optometrist will measure the reshaping of your cornea, change in your unaided vision
          and check your eye health.</p>
        <p>High prescriptions or astigmatism may take two to four weeks to get to the full
          correction level. If the treatment is going to plan, your next appointment is booked for
          one month later. If the results indicate the lenses could be improved, the parameters of
          your Forge Ortho-K lenses will be fine-tuned in EyeSpace and a new set of lenses will be
          ordered. In order to tailor the reshaping of your cornea modified lenses are covered under
          warranty for either 90 or 120 days. Ask your optometrist what the warranty is on
          yours.</p>
        <h3>Ongoing follow up</h3>
        <p><strong>Follow up appointments will be scheduled after three months, then six
          months.</strong></p>
        <p>If you experience any problems, please contact your optometrist to arrange another
          appointment.</p>
        <p>Follow up appointments are vital to the monitoring and success of your corneal reshaping.
          After the initial 12 months, we recommend yearly follow up to ensure your lenses are safe
          to continue wearing, you are seeing well, and your eye health is uncompromised.</p>
        <p>Forge Ortho-K lenses can last much longer than soft lenses. It is recommended to replace
          your lenses every one to two years to ensure they work optimally and do not cause any
          adverse health effects.</p>
        <h2>What to expect from Ortho-K?</h2>
        <h3>Treatment time</h3>
        <p>The treatment time to achieve clear and stable vision can range from between one to four
          weeks, depending on the individual. Although the lenses may feel slightly uncomfortable at
          first, this feeling decreases with time. With the eyes closed, most patients cannot feel
          their lenses.</p>
        <p>While your vision is in the process of being corrected, you may need to wear temporary
          spectacles or disposable contact lenses.</p>
        <h3>Follow-ups and troubleshooting</h3>
        <p>Regular follow-up visits are scheduled to monitor your treatment. It is crucial to attend
          these and to diligently follow instructions provided by your optometrist to ensure your
          eyes remain healthy. Lens complications are minimised when instructions are followed.
          Complications include:</p>
        <ul>
          <li>Lens binding on awakening (this can easily and safely be dislodged following the
            provided instructions)
          </li>
          <li>Corneal Staining. Due to abrasion of the surface of the eye.</li>
          <li>Fluctuating vision (if lenses have not centred properly or bubbles have been trapped
            under the lens)
          </li>
          <li>Microbial Keratitis. Infection of the cornea. Requires intensive treatment with
            topical antibiotics
          </li>
        </ul>
        <h3>Night vision</h3>
        <div class="clear"></div>
        <figure class="float-right"><img src="@/assets/images/night-vision.png" alt="Night vision"
                                         width="180"></figure>
        <p>Some patients, especially those with large pupils, notice halos and ﬂ are during the
          evening when using Ortho-K lenses.</p>
        <p>The halos are caused by the pupil enlarging in low light and allowing light rays from
          outside the Ortho-K treatment zone to enter the eye. This<br/>
          improves over the first one to two months as the Ortho-K effect stabilises. When designing
          lenses to control myopia, this phenomenon is normal and part of the treatment process.</p>
        <p>Using pupil constricting eye drops can also improve the symptoms. Our next generation
          Forge Ortho-K lens designs have wider treatment zones than ever before to improve night
          vision.</p>
        <div class="clear"></div>
        <h3>When treatment is complete</h3>
        <p>When your treatment is complete, you need to wear the final pair of Forge Ortho-K lenses
          every night to maintain your vision. Some patients with low prescriptions are even able to
          skip nights, and their vision will be maintained for 40 hours.</p>
        <p><strong>Regular aftercare visits are necessary to ensure the ongoing health of your
          eyes.</strong></p>
        <h2>Frequently asked questions</h2>
        <ul>
          <li><strong>How new is Ortho-K vision correction?</strong></li>
        </ul>
        <div class="clear"></div>
        <figure class="float-right"><img src="@/assets/images/safe-lenses.png" alt="Safe lenses"
                                         width="180"></figure>
        <p>The idea of corneal reshaping to correct myopia may have originated more than 200 years
          ago when the Samurai’s used bags of sand resting on their closed eyelids to improve their
          vision prior to going to war.</p>
        <p>Reshaping the cornea using standard rigid contact lenses was first reported in 1962 at
          the International Society of Contact Lens Specialists conference in Chicago, by George
          Jessen who described his “orthofocus” procedure. Unfortunately, this process took too
          long, and results were unpredictable. With technological advancements in lens materials
          and manufacturing, the modern reverse geometry orthokeratology lens was introduced in the
          late 1980’s.<sup>11</sup> In 1994, the United States FDA granted the first ever daily wear
          approval for Ortho-K and in June 2002, the FDA granted approval for overnight wear
          Ortho-K.</p>
        <div class="clear"></div>
        <ul>
          <li><strong>Are there age restrictions for Ortho-K?</strong></li>
        </ul>
        <p>There is no specific age restriction for Ortho-K. Children as young as five have
          successfully<br/>
          and safely received Ortho-K corneal reshaping. Ortho-K is scientifically proven to reduce
          or halt the progression of myopia, a process called myopia control and is one of the most
          popular methods of myopia control vision correction in children and
          teenagers.<sup>12,13</sup></p>
        <ul>
          <li><strong>Is Ortho-K therapy permanent?</strong></li>
        </ul>
        <p>No. If you stop wearing the Ortho-K lenses, your vision will return to its original state
          within one to four weeks.</p>
        <ul>
          <li><strong>Can the Ortho-K contact lenses damage my eyes?</strong></li>
        </ul>
        <p>Any contact lens has the potential to cause an infection in your eyes. Research shows the
          rate of infection using Ortho-K lenses is less than half compared to overnight wear of
          extended wear soft contact lenses.<sup>14</sup></p>
        <p>Using correct hygiene and lens care regimes as well as replacing the lenses every one to
          two years significantly reduces the risk of Ortho-K related complications.<sup>14</sup>
        </p>
        <p>A poorly fitted Ortho-K lens can result in corneal staining, which comprises the outer
          layer on the cornea, and increases the risk for infection. Rather than fit Ortho-K lenses
          through trial and error using a diagnostic lens from a trial case, EyeSpace software
          calculates and optimises the fit of each Forge Ortho-K lens to the topographical shape of
          the cornea.</p>
        <ul>
          <li><strong>How different are the Ortho-K contact lenses from other contact
            lenses?</strong></li>
        </ul>
        <p>Forge Ortho-K contact lenses are manufactured from hyper-Dk rigid gas permeable (RGP)
          plastics. The same lens materials are used for day wear corneal RGP lenses, however, it is
          the back surface geometry that differs. Rather than a lens geometry that aligns to the
          surface of the cornea, an Ortho-K lens has a centre zone that provides the template
          curvature that the cornea is to be reshaped to, followed a peripheral zone that creates a
          tear channel, and fits to the shape of the peripheral cornea. Another key feature is the
          diameter of a lens, day wear RGP’s are typically 8.00 to 10.00mm, Ortho-K 10.00 to
          12.00mm, and soft lenses 14.00 to 15.00mm.</p>
        <ul>
          <li><strong>Can I see with my Ortho-K contact lenses in the eye?</strong></li>
        </ul>
        <p>Yes, while the back surface of the lens reshapes your eye, the front surface of the lens
          provides clear optics for your vision. One of the great features of Ortho-K is your vision
          will always be clear whether the lenses are in or out of the eye. That means if you get up
          at night you will still be able to see. We do not recommend wearing your Ortho-K lenses
          during the day as this may compromise corneal health and vision over time.</p>
        <ul>
          <li><strong>Do I need to wear my Ortho-K lenses every night?</strong></li>
        </ul>
        <p>During the initial period of therapy, you will need to wear your lenses every night to
          ensure clear vision during the day. As the correction stabilises, some people with low
          prescriptions may still get great daytime vision by wearing the lenses only every second
          night. However, this is not guaranteed. Patients requiring higher prescription correction
          with Ortho-K will most likely need to wear the lenses every night for optimum vision.</p>
        <ul>
          <li><strong>What will happen if I forget to wear my lenses for a night?</strong></li>
        </ul>
        <p>Most people will notice a slight deterioration in the quality of their vision the next
          day due to the cornea slowly changing back to its original shape. Restarting the Ortho-K
          therapy the next night will restore your vision within a day or two.</p>
        <ul>
          <li><strong>How long does it take to reach good vision?</strong></li>
        </ul>
        <p>Most patients have rapid improvement in the first few days of therapy. Usually, after the
          first night of wear, 50% of the required correction is achieved. Optimum, stable vision
          for most will require between 10-14 days of treatment depending on the prescription.</p>
        <ul>
          <li><strong>How often will I have to replace my Ortho-K Contact lenses?</strong></li>
        </ul>
        <p>The recommendation is to replace your Ortho-K lenses every one to two years depending on
          their condition. Timely replacement of the lenses maximises the quality of the Ortho-K
          reshaping, as wear and tear will compromise its effect over time. Regular replacement
          decreases the chance of infection or inﬂ ammatory events from a scratched or dirty lens.
          You should bring your Forge Ortho-K lenses to every appointment to allow your optometrist
          to check their condition under the microscope.</p>
        <ul>
          <li><strong>Why should I not just get laser vision correction?</strong></li>
        </ul>
        <p>The cost of Ortho-K vision correction is much less than the cost of laser vision
          correction. Ortho-K is completely reversible and does not have the potential to worsen
          your dry eyes to the same extent as laser correction. Also, as patients reach their
          mid-forties and start needing help for their near vision, Ortho-K therapy can be modified
          to give clear vision at all distances without a pair of glasses, unlike a one-offlaser
          procedure.</p>
        <ul>
          <li><strong>How long does the myopia control effect of Ortho-K last?</strong></li>
        </ul>
        <p>The myopia control effect will last as long as you use your Ortho-K lenses. There is no
          research or anecdotal reports of patients ‘rebounding’ after ceasing Ortho-K contact
          lenses. It’s expected that patients who start Ortho-K contact lens wear for myopia control
          continue to use these until their early twenties when myopia tends to stabilise. The
          decision to stop Ortho-K should be made with your optometrist, and prescription and eye
          length should be closely monitored for possible regression. In most cases, patients are so
          happy with their Ortho-K lenses that they continue to wear them well into adulthood!</p>
        <ul>
          <li><strong>What costs are not included in my Ortho-K fitting program?</strong></li>
        </ul>
        <p>You will need purchase solutions to store and clean your lenses, as well as lubricating
          drops for lens insertion.</p>
        <ul>
          <li><strong>How do I get started?</strong></li>
        </ul>
        <p>A full eye exam is required. The overall health of the eye, as well as vision and corneal
          shape, will be assessed to establish if your eyes are suitable.</p>
        <ol>
          <li><a href="https://www.orthokacademy.com/faq-for-patients/">https://www.orthokacademy.com/faq-for-patients/</a>.
          </li>
          <li>Morphologic changes in cat epithelium following continuous wear of orthokeratology
            lenses: a pilot study. 2008 Feb;31(1):29–37.
          </li>
          <li>Chan B, Cho P, de Vecht A. Toric orthokeratology: a case report. Clin Exp Optom.
            Blackwell Publishing Asia; 2009 Jul;92(4):387–91.
          </li>
          <li>Gifford P, Swarbrick HA. Time course of corneal topographic changes in the first week
            of overnight hyperopic orthokeratology. Optom Vis Sci. 2008 Dec;85(12):1165–71.
          </li>
          <li>Gifford P, Swarbrick HA. Refractive changes from hyperopic orthokeratology monovision
            in presbyopes. Optom Vis Sci. 2013 Apr;90(4):306–13.
          </li>
          <li>Chen C, Cho P. Toric orthokeratology for high myopic and astigmatic subjects for
            myopic control. Clin Exp Optom. 2012 Jan;95(1):103–8.
          </li>
          <li>Effect of orthokeratology in patients with myopic regression after refractive surgery.
            2016 Apr;39(2):167–71.
          </li>
          <li>Recovery of corneal irregular astigmatism, ocular higher-order aberrations, and
            contrast sensitivity after discontinuation of overnight orthokeratology. 2009
            Feb;93(2):203–8.
          </li>
          <li>Bullimore MA, Sinnott LT, Jones-Jordan LA. The risk of microbial keratitis with
            overnight corneal reshaping lenses. Optom Vis Sci. 2013 Sep;90(9):937–44.
          </li>
          <li>Choroidal thickness and axial length changes in myopic children treated with
            orthokeratology. Contact Lens and Anterior Eye 40 (2017) 417–423.
          </li>
          <li>Book: Orthokeratology Principles and practice by John Mountford, David Ruston Trusit
            Dave
          </li>
          <li>Huang J, Wen D, Wang Q, McAlinden C, Flitcroft I, Chen H, et al. Eﬃ cacy Comparison of
            16 Interventions for Myopia Control in Children: A Network Meta-analysis. Ophthalmology.
            2016 Apr;123(4):697–708
          </li>
          <li>Turnbull PRK, Munro OJ, Phillips JR. Contact Lens Methods for Clinical Myopia Control.
            Optom Vis Sci. 2016 Sep;93(9):1120–6.
          </li>
          <li>Liu YM, Xie P. The Safety of Orthokeratology--A Systematic Review. Eye Contact Lens.
            2016 Jan;42(1):35–42.
          </li>
        </ol>
      </article>
    </section>
  </div>
</template>

<script>
    export default {
        name: 'what-is-forge-ortho-k',
    };
</script>

<style scoped>

</style>
